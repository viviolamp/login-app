import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "./Pages/Login";
import Home from "./Pages/Home";
import Navigation from "./Navigation";
import Logout from "./Pages/Logout";

function App() {
  return (
    <Router>
      <div>
        <h1>Login App</h1>
        <hr />
        <Navigation />
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/" exact={true}>
            <Home />
          </Route>
          <Route path="/logout">
            <Logout />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
