import Cookies from "js-cookie";

export function checkLogin() {
  if (typeof Cookies.get("token") === "undefined") {
    console.log("belum login");
    return false;
  } else {
    console.log("sudah login");
    return true;
  }
}
