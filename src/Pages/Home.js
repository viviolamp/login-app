import { checkLogin } from "../Helper";

function Home() {
  return <div>{checkLogin() ? "sudah login" : "belum login"}</div>;
}

export default Home;
