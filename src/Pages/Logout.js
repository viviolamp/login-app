import Cookies from "js-cookie";
import { useHistory } from "react-router-dom";

export default function Logout() {
  let history = useHistory();
  Cookies.remove("token");
  history.push("/");

  return null;
}
